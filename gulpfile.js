"use strict";
const gulp = require('gulp'),
    sass = require('gulp-sass'),
    prefixer = require('gulp-autoprefixer'),
    plumber = require('gulp-plumber'),
    htmlmin = require('gulp-htmlmin'),
    browserSync = require('browser-sync'),
    reload = browserSync.reload,
    rimraf = require('rimraf');

const path = {
  build:{
      html:'build/',
      scss:'build/css/',
      js:'build/js/'
  },
  src:{
      html:'src/*.{html,htm}',
      scss:'src/scss/main.scss',
      js:'src/js/*.js'
  },
  watch:{
      html:'src/*.{html,htm}',
      scss:'src/scss/**/*.scss',
      js:'src/js/**/*.js'
  },
  clean: {
      all: 'build'
    }
  };

 const config = {
        server: {
            baseDir: './build',
            index: 'index.html'
        },
        host: 'localhost',
        port:'7787',
        tunnel:true
};

//удалить директорию со сборкой
gulp.task("clean",function (done) {
    rimraf(path.clean.all,done);
});

//задача сборки scss в в папку сборки сss
gulp.task("dev:scss",function (done) {
    gulp.src(path.src.scss)
        .pipe(plumber())
        .pipe(sass({
            outputStyle: "expanded" // тип вывода css
        }
        ))
        .pipe(gulp.dest(path.build.scss,{sourcemaps:'.'}))
        .pipe(reload({stream: true}));
    done();
});

//задача переноса html в папку сборки
gulp.task("dev:html",function (done) {
  gulp.src(path.src.html)
        .pipe(gulp.dest(path.build.html))
        .pipe(reload({stream: true}));
    done();
});

//задача для теста и проверки передаваемых объектов
gulp.task("test",function (done) {
    gulp.src('src/*.html')
        .on('data',function (file) {
            console.log({//VynilFS object
               contents:file.contents,
               path:file.path,
               cwd:file.cwd,
               base:file.base,
               //helpers
               dirname:file.dirname,
               relative:file.relative,
               basename:file.basename,
               stem:file.stem,
               extname:file.extname
            });
        })
        .pipe(gulp.dest('build/'));
    done();
});

// следим за изменениями в html И scss
gulp.task('watch', function (done) {
    gulp.watch(path.watch.html, gulp.series('dev:html'));
    gulp.watch(path.watch.scss, gulp.series('dev:scss'));
    done();
    }
);

// запускаем webserver
gulp.task('webserver', function(done){
    browserSync(config);
    done();
});

//задача по умолчанию
gulp.task("default",
    gulp.series(
        'clean',
        gulp.parallel(
            'dev:html',
            'dev:scss'
        ),
        'webserver',
        'watch'
    )
);